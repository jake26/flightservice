package com.jktairlines.flightservice;

import com.jktairlines.flightservice.model.Flight;
import com.jktairlines.flightservice.service.IFlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Date;

@SpringBootApplication
public class FlightServiceApplication {

	@Autowired
	IFlightService iFlightService;

	public static void main(String[] args) {
		SpringApplication.run(FlightServiceApplication.class, args);
	}

	@Bean
	public CommandLineRunner initSampleData() {
		return (args) -> {
			System.out.println("Init sample data...");

			iFlightService.addFlight(new Flight("QF100", "ABC", "DEF", new Date(), new Date()));
			iFlightService.addFlight(new Flight("QF200", "EFG", "HIJ", new Date(), new Date()));
			iFlightService.addFlight(new Flight("EK200", "LMN", "OPQ", new Date(), new Date()));
			iFlightService.addFlight(new Flight("EK300", "LMN", "OPQ", new Date(), new Date()));
			iFlightService.addFlight(new Flight("CX300", "LMN", "OPQ", new Date(), new Date()));
			iFlightService.addFlight(new Flight("CX200", "LMN", "OPQ", new Date(), new Date()));
		};
	}
}
