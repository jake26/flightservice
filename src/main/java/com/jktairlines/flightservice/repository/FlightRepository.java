package com.jktairlines.flightservice.repository;

import com.jktairlines.flightservice.model.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("flightRepository")
public interface FlightRepository extends JpaRepository<Flight, Long> {
    List<Flight> findByFlightNumberStartsWith(String airlineCode);
}
