package com.jktairlines.flightservice.rest;

import com.jktairlines.flightservice.model.Flight;
import com.jktairlines.flightservice.service.IFlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/flights")
public class FlightResource {
    @Autowired
    IFlightService iFlightService;


    @GetMapping
    public ResponseEntity<List<Flight>> getFlights() {
        return new ResponseEntity<>(iFlightService.getFlights(), HttpStatus.OK);
    }

    @GetMapping("/{airlineCode}")
    public ResponseEntity<List<Flight>> getFlightsByAirlineCode(@PathVariable("airlineCode") String airlineCode) {
        return new ResponseEntity<>(iFlightService.getFlightsByAirlineCode(airlineCode), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Flight> addFlight(@RequestBody Flight flight) {
        return new ResponseEntity<>(iFlightService.addFlight(flight), HttpStatus.CREATED);
    }

}
