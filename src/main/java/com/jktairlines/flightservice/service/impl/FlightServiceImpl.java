package com.jktairlines.flightservice.service.impl;

import com.jktairlines.flightservice.model.Flight;
import com.jktairlines.flightservice.repository.FlightRepository;
import com.jktairlines.flightservice.service.IFlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class FlightServiceImpl implements IFlightService {

    @Autowired
    private FlightRepository flightRepository;

    @Override
    @Transactional
    public List<Flight> getFlights() {
        return flightRepository.findAll();
    }

    @Override
    @Transactional
    public List<Flight> getFlightsByAirlineCode(String airlineCode) {
        return flightRepository.findByFlightNumberStartsWith(airlineCode);
    }

    @Override
    @Transactional
    public Flight addFlight(Flight flight) {
       return flightRepository.saveAndFlush(flight);
    }

}
