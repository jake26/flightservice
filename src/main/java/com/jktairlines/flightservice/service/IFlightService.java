package com.jktairlines.flightservice.service;

import com.jktairlines.flightservice.model.Flight;

import java.util.List;

public interface IFlightService {
    public List<Flight> getFlights();
    public List<Flight> getFlightsByAirlineCode(String airlineCode);
    public Flight addFlight(Flight flight);
}
