package com.jktairlines.flightservice.service;

import com.jktairlines.flightservice.model.Flight;
import com.jktairlines.flightservice.repository.FlightRepository;
import com.jktairlines.flightservice.service.impl.FlightServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FlightServiceImplTest {
    @InjectMocks
    FlightServiceImpl flightService;

    @Mock
    FlightRepository flightRepository;

    @Test
    public void whenGetFlights_thenReturnFlights() {
        List<Flight> expectedFlights = new ArrayList<>();
        Flight flight1 = new Flight("TS100", "ABC", "DEF", new Date(), new Date());
        Flight flight2 = new Flight("TS200", "EFG", "HIJ", new Date(), new Date());
        Flight flight3 = new Flight("TS300", "LMN", "OPQ", new Date(), new Date());

        expectedFlights.add(flight1);
        expectedFlights.add(flight2);
        expectedFlights.add(flight3);

        doReturn(expectedFlights).when(flightRepository).findAll();
        // when
        List<Flight> actualFlights = flightService.getFlights();

        // then
        assertThat(actualFlights).isEqualTo(expectedFlights);
    }

    @Test
    public void whenGetFlightsByAirlineCode_thenReturnFlightsByAirlineCode() {
        List<Flight> expectedFlights = new ArrayList<>();
        Flight flight1 = new Flight("TS100", "ABC", "DEF", new Date(), new Date());
        Flight flight2 = new Flight("TS200", "EFG", "HIJ", new Date(), new Date());

        expectedFlights.add(flight1);
        expectedFlights.add(flight2);

        doReturn(expectedFlights)
                .when(flightRepository)
                .findByFlightNumberStartsWith("TS");

        List<Flight> actualFlights = flightService.getFlightsByAirlineCode("TS");

        assertThat(actualFlights).isEqualTo(expectedFlights);
    }

    @Test
    public void whenAddFlight_thenReturnAddedFlight(){
        Flight newFlight = new Flight("TS100", "ABC", "DEF", new Date(), new Date());
        flightService.addFlight(newFlight);
        verify(flightRepository, times(1)).saveAndFlush(newFlight);
    }
}
