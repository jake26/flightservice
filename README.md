# JKT Airlines Flight API Service #

REST API for flights using Spring Boot, JPA, H2 Database and Hibernate

### Requirements ###

* [JDK 1.8](https://www.oracle.com/ph/java/technologies/javase/javase-jdk8-downloads.html)
* [Maven](https://maven.apache.org/download.cgi)

### Running the application locally ###

**1. Clone the project**
```bash
git clone https://jake26@bitbucket.org/jake26/flightservice.git
```
####
**2. Go to project directory**
####
**3. Build and run the app using maven**
####

```bash
mvn package
java -jar target/flightservice-0.0.1-SNAPSHOT.jar
```

Alternatively, you can run the app without packaging it using:

```bash
./mvnw spring-boot:run
```

The app will start running at <http://localhost:8080>.

**AVAILABLE ENDPOINTS**

| method            | resource                  | description                                                                                   |
|:------------------|:--------------------------|:----------------------------------------------------------------------------------------------|
| `GET`			    | `/flights`		        | get collection of flights -> 200(OK)														|
| `GET`			    | `/flights/:airline_code`	| get collection of flights by airline code -> 200(OK)		|
| `POST`			| `/flights`		            | add new flight -> 201(CREATED)														        |